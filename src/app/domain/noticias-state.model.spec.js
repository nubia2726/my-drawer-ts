var noticias_state_model = require("./noticias-state.model");

describe("reducersNoticias", function(){
    it("should reduce init data", function(){
        //set up
        var prevState = noticias_state_model.intializeNoticiasState();
        var action = new noticias_state_model.initMyDataAction(["noticias 1", "noticias 2"]);
        //action
        var newState = noticias_state_model.reducersNoticias(prevState, action);
        //assertions
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].titulo).toEqual("noticia 1");
    });
    it("should reduce new item added", function (){
        var prevState = noticias_state_model.intializeNoticiasState();
        var action = new noticias_state_model.NuevaNoticiaAction(new noticias_state_model.Noticia("noticia 3"));
        var newState = noticias_state_model.reducersNoticias(prevState, action);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].titulo).toEqual("noticia 3");
    });
    it("should reduce new item added v2", function (){
        var prevState = noticias_state_model.intializeNoticiasState();
        var action = new noticias_state_model.NuevaNoticiaAction(new noticias_state_model.Noticia("noticia 3"));
        var newState = noticias_state_model.reducersNoticias(prevState, action);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].titulo).toEqual("noticia 3");
    });
});
