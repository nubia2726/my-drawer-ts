import { Component, OnInit } from '@angular/core';
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';
import * as app from "tns-core-modules/application";


@Component({
    selector: 'ns-infodetalles',
    templateUrl: './infodetalles.component.html'

})
export class InfodetallesComponent implements OnInit {

    constructor( ) { }

    ngOnInit() {


    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}



