import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { Store } from "@ngrx/store";
import * as SocialShare from "nativescript-social-share";
import * as Toast from "nativescript-toasts";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { AppState } from "./../../app.module";
import { Noticia, NuevaNoticiaAction } from "./../../domain/noticias-state.model";
import { NoticiasService } from "./../../domain/noticias.service";
import { RouterExtensions } from "nativescript-angular/router";
import { Color, View, layout } from "tns-core-modules/ui/core/view/view";
@Component({
    selector: "Search",
    moduleId: module.id,
    templateUrl: "./search.component.html" /*,
    providers: [NoticiasService]*/
})

export class SearchComponent implements OnInit {
    resultados : Array<string>;
    @ViewChild ("layout", null) layout:ElementRef;
    store: any;


    constructor(
        private noticias: NoticiasService,
        private Store: Store<AppState>,
        private routerExtensions: RouterExtensions) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        this.store.select((state) => state.noticias.sugerida)
        .subscribe((data) => {
            const f = data;
            if(f != null) {
                Toast.show({text: "Sugerimos leer: " + f.titulo, duration: Toast.DURATION.SHORT});
            }
        });

    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(args):void{
        this.store.dispatch(new NuevaNoticiaAction(new Noticia(args.view.bindingContext)));
    }

    onLongPress(s) {
        SocialShare.shareText(s, "Asunto compartido desde el curso");
    }

    onNavItemTap(navItemRoute: string): void {
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            }
        });

        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.closeDrawer();
    }

    buscarAhora(s:string){
        console.dir("buscarAhora" + s);
        this.noticias.buscar(s).then((r: any) =>{
            console.log("resultados buscarAhora: " + JSON.stringify(r));
            this.resultados = r;
        }, (e) => {
            console.log("error buscarAhora" + e);
            Toast.show({text: "Error en la busqueda", duration: Toast.DURATION.SHORT});
        });
    }
}
