import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { DetallesnotiComponent } from "./detallesnoti.component";

const routes: Routes = [
    { path: "", component: DetallesnotiComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class DetallesnotiRoutingModule { }
